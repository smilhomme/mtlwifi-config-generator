#!/usr/bin/env python
import sys
import json
import os
import math
import re


parameters = []

if (len(sys.argv) < 3):
  print 'Usage: wifi_csv_to_json input_CSV_file output_JSON_file'
  quit()

# 1. read the contents from the CSV files
#print("Read CSV parameter file {0}".format(sys.argv[1]))
f = open(sys.argv[1],'r')
csv_content = f.read()
f.close()

#print("Convert CSV file to dictionaries...")
csv_lines = csv_content.splitlines()
headers = csv_lines[0].split(";")

j=0
print "---"
print "interfaces:"

for i in range(1, len(csv_lines)):
    values = csv_lines[i].split(";")
    item = dict()
    for h in range(0, len(headers)):
        try:
            parsed_value = int(values[h])
        except ValueError:
            try:
                parsed_value = float(values[h])
            except ValueError:
                parsed_value = values[h]
            #parsed_value = values[h]
        item[headers[h]] = parsed_value

    """
    SEQUENCE
    """
    lot_id = int(item["LOT_ID"])
    id_ring = int(item["ID_RING"])
    id_node = int(item["ID_NODE"])
    nb_node = int(item["NB_NODE"])

    id_seq = (lot_id-1)*24*8 + id_ring*8 + id_node
    item["SEQ"] = id_seq

    """
    Subnets Transport
    10.255.192.0/18 = 10.255.192.0 - 10.255.255.254
    10.255.{f(Ring_ID)}.{f(Node_ID)}/31
    """

    t_thirdbyte = 192 + int(math.floor((lot_id-1)*2 + id_ring/12))

    t_fourthbyte_A = (id_ring%12)*20 + id_node*2 + 1
    t_fourthbyte_B = (id_ring%12)*20 + id_node*2 + 2

    item["IP_A"] = "10.255." + str(t_thirdbyte) + "." + str(t_fourthbyte_A)
    item["IP_B"] = "10.255." + str(t_thirdbyte) + "." + str(t_fourthbyte_B)

    """
    Subnet AP
    10.52.0.0/15
    10.{f(Lot_ID)}.{f(Lot_ID,Ring_ID)}.{f(Node_ID)}/27
    2: 52 + floor((lot_ID-1)/10)
    3: (Lot-1)x24 + Ring_ID
    4: Node_IDx32 + 1
    """
    apsecondbyte = int(52 + math.floor((lot_id - 1)/10))
    apthirdbyte = ((lot_id-1)%10)*24 + id_ring

    item["SUBNET_AP"] = "10." + \
    str(apsecondbyte) + "." + \
    str(apthirdbyte) + "." + \
    str(id_node*32 + 1)

    """
    Loopback
    10.53.240.0/20
    10.53.{Ring_ID}.{f(Node_ID)}/32

    """
    loopback_thirdbyte = int(240 + math.floor(id_seq/256))
    loopback_fourthbyte = int(id_seq % 256)
    item["IP_LOOPBACK"] = "10.53." + str(loopback_thirdbyte) + "." + str(loopback_fourthbyte)

    """
    IP_DEPLOY
    10.255.240.0/20
    10.255.{Ring_ID}.{f(Node_ID)}/32

    """
    loopback_thirdbyte = int(240 + math.floor(id_seq/256))
    loopback_fourthbyte = int(id_seq % 256)
    item["IP_DEPLOY"] = "10.255." + str(loopback_thirdbyte) + "." + str(loopback_fourthbyte)


    IP_VIGER_1 = "*"
    IP_VIGER_2 = "*"

    """
    ---
    interfaces:
    "gi1/0/1":
    - { lot_name: QDC, id_ring: 0, pair: 25, ip_addr: 10.255.200.0 }
    """
    if id_node == 0 :
        v_thirdbyte = 192 + int(math.floor((lot_id-1)*2 + id_ring/12))
        v_fourthbyte_A = (id_ring%12)*20
        IP_VIGER_1 = "10.255." + str(v_thirdbyte) + "." + str(v_fourthbyte_A)
        IP_VIGER_2 = ""
        j = j + 1
        print '  "GigabitEthernet 1/0/' + str(j) + '":'
        print '    - { lot_name: "' +item["LOT_NAME"] + '", id_map: "'+str(item["ID_MAP"])+ '", id_ring: '+ str(id_ring)+', pair: '+str(item["PAIR"])+', ip_addr: "'+IP_VIGER_1+'" }'

    if nb_node - id_node - 1 == 0 :
        v_thirdbyte = 192 + int(math.floor((lot_id-1)*2 + id_ring/12))
        v_fourthbyte_B = (id_ring%12)*20 + (nb_node-1)*2 + 3
        IP_VIGER_2 = "10.255." + str(v_thirdbyte) + "." + str(v_fourthbyte_B)
        IP_VIGER_1 = ""
        print '  "GigabitEthernet 2/0/' + str(j) + '":'
        print '    - { lot_name: "' +item["LOT_NAME"] + '", id_map: "'+str(item["ID_MAP"])+ '", id_ring: '+ str(id_ring)+', pair: '+str(item["PAIR"])+', ip_addr: "'+IP_VIGER_2+'" }'

    #print str(item["ID_MAP"]) + "\t" + str(item["SEQ"]) + "\t" + str(item["LOT_ID"]) + "\t" + str(item["ID_RING"]) + "\t" + \
    #str(item["ID_NODE"]) + "\t" + item["IP_A"]+ "\t" + item["IP_B"]+ \
    #"\t" + item["SUBNET_AP"] + "\t" + item["IP_LOOPBACK"] + "\t" + IP_VIGER_1 + "\t" + IP_VIGER_2

    parameters.append(item)

#re.sub(r'(?<!: )"(\S*?)"', '\\1', parameters)

#print("Saving to JSON file {0}".format(sys.argv[2]))

f = open(sys.argv[2],'w')
json.dump(parameters, f, indent=4, sort_keys=True)
f.close()

#print("DONE")
