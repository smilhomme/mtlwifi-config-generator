#!/usr/bin/env python
import sys
import json
import os
import math
import re


parameters = []

if (len(sys.argv) < 3):
  print 'Usage: wifi_csv_to_json input_CSV_file output_JSON_file'
  quit()

# 1. read the contents from the CSV files
print("Read CSV parameter file {0}".format(sys.argv[1]))
f = open(sys.argv[1],'r')
csv_content = f.read()
f.close()
j = 1
print("Convert CSV file to dictionaries...")
csv_lines = csv_content.splitlines()
headers = csv_lines[0].split(";")

#item = dict()
for i in range(1, len(csv_lines)):
    values = csv_lines[i].split(";")
    item = dict()
    for h in range(1, 8):
        try:
            parsed_value = int(values[h])
        except ValueError:
            try:
                parsed_value = float(values[h])
            except ValueError:
                parsed_value = values[h]
        item[headers[h]] = parsed_value

    lot_id = int(item["LOT_ID"])
    id_ring = int(item["ID_RING"])
    id_node = int(item["ID_NODE"])
    nb_node = int(item["NB_NODE"])

    if id_node == 0 :

        """
        Subnets Transport
        10.255.192.0/18 = 10.255.192.0 - 10.255.255.254
        10.255.{f(Ring_ID)}.{f(Node_ID)}/31
        """

        t_thirdbyte = 192 + int(math.floor((lot_id-1)*2 + id_ring/12))

        t_fourthbyte_A = (id_ring%12)*20
        t_fourthbyte_B = (id_ring%12)*20 + (nb_node-1)*2 + 1

        item["GigabitEthernet1/0/" + str(j)] = "10.255." + str(t_thirdbyte) + "." + str(t_fourthbyte_A)
        item["GigabitEthernet2/0/" + str(j)] = "10.255." + str(t_thirdbyte) + "." + str(t_fourthbyte_B)

        j = j + 1

        

        parameters.append(item)


print("Saving to JSON file {0}".format(sys.argv[2]))

f = open(sys.argv[2],'w')
json.dump(parameters, f, indent=4, sort_keys=False)
f.close()

print("DONE")
