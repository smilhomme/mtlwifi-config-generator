#!/usr/bin/env python
import sys
import csv

if (len(sys.argv) < 2):
  print 'Usage: %s ' % sys.argv[0]
  quit()
 
csv_file = sys.argv[1]
 
filename, suffix = csv_file.split('.')
reader = csv.reader(file(csv_file, 'r'), delimiter=argv[1])

i = -1
res = []
for row in reader:
  i += 1
  if i == 0:
    column = row
    continue

  s = filename + str(i) + ":\n"
  for j in range(len(column)):
    s += "  " + column[j] + ": " + row[j]  + "\n"
  res.append(s)

f = open(filename + ".yml", 'w')
f.write("\n".join(res))
f.close()
