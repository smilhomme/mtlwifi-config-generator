import json
import os


template_file = "switch.j2"
csv_parameter_file = "wifi_switches.csv"
config_parameters = dict()
output_directory = "_output"
output_json = "wifi_switches.json"

FIBRE = dict()

general_parameters = {
    'username': 'localadmin',
    'localadmin_password': 'cisco',
    'domain_sufix': 'mtlwifi.vdmtl.qc.ca',
    'syslog_server_1': '10.55.0.1',
    'snmp_server_1': '10.55.0.1',
    'snmp_server_2': '10.55.0.2',
    'snmp_community': 'QWERTY',
    'tacacs_server_1': '10.55.0.1',
    'tacacs_server_2': '10.55.0.2',
    'radius_key': 'QWERTY',
    'helper_server_1': '10.55.0.1',
    'helper_server_2': '10.55.0.2',
    'ntp_server': '10.55.0.1',
    'interface_AP': '1/5',
    'vlan_AP': 123,
    'subnet_transport': '172.18.0.0'
}

"""
# 1. read the contents from the CSV files
print("Read CSV parameter file...")
f = open(csv_parameter_file)
csv_content = f.read()
f.close()

# 2. for Jinja2, we need to convert the given CSV file into the a python
# dictionary to get the script a bit more reusable, I will not statically
# limit the possible header values (and therefore the variables)
print("Insert general parameters to dictionaries...")
config_parameters['general_parameters'] = general_parameters

print("Convert CSV file to dictionaries...")
csv_lines = csv_content.splitlines()
headers = csv_lines[0].split(";")
for i in range(1, len(csv_lines)):
    values = csv_lines[i].split(";")
    parameter_dict = dict()
    for h in range(0, len(headers)):
        parameter_dict[headers[h]] = values[h]


    FIBRE['LOT_ID'] = parameter_dict['LOT_ID']
    FIBRE['LOT_NAME'] = parameter_dict['LOT_NAME']
    for h in range(2, 5):
        FIBRE[parameter_dict['LOT_ID']][parameter_dict['ID_RING']] = { headers[h]: parameter_dict[headers[h]] }
        #FIBRE = { 'RINGS': { headers[h] : parameter_dict[headers[h]] }}

    for h in range(6, len(headers)):
        FIBRE[parameter_dict['LOT_ID']][parameter_dict['ID_RING']][parameter_dict['ID_NODE']] = {headers[h]: parameter_dict[headers[h]]}

print FIBRE

print("Saving to JSON file...")

f = open(output_json,'w')
json.dump(config_parameters,f,indent=4,sort_keys=True)
json.dump(FIBRE,f,indent=4,sort_keys=True)
f.close()
"""
f = open('general_parameters.json','w')
json.dump(general_parameters,f,indent=4,sort_keys=True)

print("DONE")